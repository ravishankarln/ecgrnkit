import Foundation
import CoreBluetooth
import QuartzCore
import CoreData

extension String {
  var ns: NSString {
    return self as NSString
  }
}

extension DateFormatter
{
  func setLocal() {
    self.locale = Locale.init(identifier: "en_US_POSIX")
    self.timeZone = TimeZone(abbreviation: "UTC")!
  }
}

struct ECGCheckmeData {
  
  var arr_ecg_content: String = ""
  var arr_ecg_heartrate: String = ""
  var ecg_result: String = ""
  var hr: String = ""
  var qrs: String = ""
  var qt: String = ""
  var qtc: String = ""
  var readingDateTime: String = ""
  var selectedUserID: String = ""
  var selectUserName: String = ""
  var selectedUserPhone: String = ""
  var deviceUser: String = ""
  var deviceIdentifire: String = ""
  var readingNotes: String = ""
  
}

@objc(EcgKit)
class EcgKit: RCTEventEmitter {
  private typealias `Self` = EcgKit
  
  static let MODULE_NAME = "EcgKit"
  
  static let INIT_ERROR = "INIT_ERROR"
  static let PAIR_ERROR = "PAIR_ERROR"
  static let UNKNOWN_ERROR = "UNKNOWN_ERROR"
  
  static let UNKOWN_ERROR_MSG = "Unknown runtime Objective-C error (EcgKit)."
  
  static let DATA_EVENT = "data"
  static let DEVICE_FOUND_EVENT = "deviceFound"
  static let DEVICE_CONNECTED_EVENT = "deviceConnected"
  static let DEVICE_DISCONNECTED_EVENT = "deviceDisconnected"
  static let AMBIGUOUS_DEVICE_FOUND_EVENT = "ambiguousDeviceFound"
  static let SCAN_FINISHED_EVENT = "scanFinished"
  static let COLLECTION_FINISHED_EVENT = "collectionFinished"
  
  var ecgDataArray : [ECGCheckmeData] = []
  var arrNewECGData = [ECGCheckmeData]()
  var mappedEcgDATA = [Any]()
  
  var arrBLEList = [CBPeripheral]()
  var macAddress = [String]()
  
  override func constantsToExport() -> [AnyHashable : Any] {
    return [
      "EVENTS": supportedEvents()
    ]
  }
  
  private func emitEvent(_ eventName: String, withData data: Any?) {
    print(Self.MODULE_NAME, "Send \(eventName) event.")
    sendEvent(withName: eventName, body: data)
  }
  
  private func mapDeviceDescription(_ device: CBPeripheral) -> [String: Any] {
//<CBPeripheral: 0x282bdc780, identifier = 373EC291-EDB1-B6F1-B668-105F06C3BBA0, name = MedCheck 0038, state = disconnected>
    return [
      "id": device.identifier.description,
      "name": device.name?.description ?? "Unknown",
      "state": device.state.rawValue
//      "modelName": device.modelName,
//      "manufacturer": device.manufacturer
    ]
  }
  
  private func mapDataDescription(_ ecgData: ECGCheckmeData ) -> [String: Any] {
    //<CBPeripheral: 0x282bdc780, identifier = 373EC291-EDB1-B6F1-B668-105F06C3BBA0, name = MedCheck 0038, state = disconnected>
    return [
//      "reading": ecgData.identifier.description,
//      "name": ecgData.name?.description,
//      "state": ecgData.state.rawValue
      
      "arr_ecg_content":ecgData.arr_ecg_content,
      "arr_ecg_heartrate":ecgData.arr_ecg_heartrate,
      "ecg_result":ecgData.ecg_result,
      "hr":ecgData.hr,
      "qrs":ecgData.qrs,
      "qt":ecgData.qt,
      "qtc":ecgData.qtc,
      "readingDateTime":ecgData.readingDateTime,
      "selectedUserID":ecgData.selectedUserID,
      "selectUserName":ecgData.selectUserName,
      "selectedUserPhone":ecgData.selectedUserPhone,
      "deviceUser":ecgData.deviceUser,
      "deviceIdentifire":ecgData.deviceIdentifire,
      "readingNotes":ecgData.readingNotes
    ]
  }
  
  func saveECGDataToCoreData(ecg: ECGCheckmeData) {
    
    var ecgData = ECGCheckmeData()
    
    
    
    ecgData.arr_ecg_content = ecg.arr_ecg_content
    ecgData.arr_ecg_heartrate = ecg.arr_ecg_heartrate
    ecgData.ecg_result = ecg.ecg_result
    ecgData.hr = ecg.hr
    ecgData.qrs = ecg.qrs
    ecgData.qt = ecg.qt
    ecgData.qtc = ecg.qtc
    ecgData.readingDateTime = ecg.readingDateTime
    ecgData.selectedUserID = ecg.selectedUserID
    ecgData.selectUserName = ecg.selectUserName
    ecgData.selectedUserPhone = ecg.selectedUserPhone
    ecgData.deviceUser = ecg.deviceUser
    ecgData.deviceIdentifire = ecg.deviceIdentifire
    ecgData.readingNotes = ecg.readingNotes
    
    self.arrNewECGData.append(ecgData)
    mappedEcgDATA.append(mapDataDescription(ecgData))
    
  }
  
//  @objc func deleteECGData() {
//    let context = self.persistentContainer.viewContext
//    let deleteRequest = NSBatchDeleteRequest(fetchRequest: ECGData.fetchRequest())
//    do {
//      try context.execute(deleteRequest)
//      try context.save()
//    } catch {
//      print ("There was an error")
//    }
//  }
  
//  @objc func fetchECGData() {
//    let context = self.persistentContainer.viewContext
//    do {
//      ecgDataArray = try context.fetch(ECGData.fetchRequest())
//      print(ecgDataArray)
//    } catch {
//      //print("Fetching Failed")
//    }
//  }
  
  @objc func initialize(_ resolve: RCTPromiseResolveBlock,
                        rejecter reject: RCTPromiseRejectBlock) {
    do {
      try ObjC.catchException {
        // TODO:- WARNING: HANDLE WHEN NO PERMISSION GIVEN FOR FISRT TIME
        // TODO:- WARNING: UPDATE REQUIRED FOR iOS 13 and ahead didDiscoverPeripheral method in BTUtils.m
        BTUtils.getInstance()!.openBT()
      }
      resolve(["init" : true])
    } catch {
      let msg = "Incorrect Device."
      reject(Self.INIT_ERROR, msg, error)
    }
  }
  
  //MARK:- findPeriphral ECG
  @objc func findPeriphral(nrf:NSNotification) {
    if let info = nrf.userInfo as? [String : Any] {
      if let periphral = info[KEYPERIPHRAL] as? CBPeripheral, let bleName = info["BLEName"] as? String {
        if !bleName.isEmpty && periphral.name! == bleName {
          periphral.setValue(bleName, forKey: "name")
        }
        
        if periphral.name!.hasPrefix("Checkme") || periphral.name!.hasPrefix("MedCheck") {
          for (index,lp) in self.arrBLEList.enumerated() where lp.name == periphral.name {
            self.arrBLEList.remove(at: index)
            
            if let i = self.macAddress.firstIndex(where: {$0 == periphral.name!}) {
              self.macAddress.remove(at: i)
            }
          }
          self.macAddress.append(periphral.name!)
          self.arrBLEList.append(periphral)
          self.emitEvent(Self.DEVICE_FOUND_EVENT, withData: self.mapDeviceDescription(periphral))
        }
//        print("ARRAY", self.arrBLEList)
        
      }
    }
  }
  
  //TODO:- ECG is connected, Sending event to RN.
  @objc func onConnectCheckmeSuccessNtf(nrf:NSNotification) {
    BTUtils.getInstance()!.stopScan()
    self.emitEvent(Self.DEVICE_CONNECTED_EVENT, withData: self.mapDeviceDescription((BTUtils.getInstance()?.currentPeripheral.peripheral)!))
    self.emitEvent(Self.SCAN_FINISHED_EVENT, withData: nil)
//    do {
//      print("Connected Device", BTUtils.getInstance()!.current  Peripheral.peripheral)
//
//    } catch {
//      print("Error while fetching Connected Device", error)
//    }
//    appDelegate.window?.rootViewController?.dismiss(animated: true, completion: {})
  }
  
  // TODO: - FETCH DATA FROM ECG MACHINE
  @objc func syncDataFromDevice(_ type: String) {
    if BTUtils.getInstance()!.deviceConnected {
      //ECGReadData.getInstance().syncTime()
      switch type {        //type
      case "ecg":
        ECGReadData.getInstance().ecgData()
        ECGReadData.getInstance().blockECGDataComplition = {(success, data, type) -> Void in
          ////print(data)
          if let arr = data as? [ECGInfoItem]{
            
            var ecgDateArray = [DateComponents]()
            
            for ecgItem in arr {
              ecgDateArray.append(ecgItem.dtcDate!)
            }
            
            if !ecgDateArray.isEmpty {
              //Show Loader
              self.getECGInnerDetails(dateCArray: ecgDateArray, index: 0)
            }
          }
        }
        break;
      default:
        break
      }
    }
  }
  
  func getDateStrFromDateComponent(date : DateComponents) -> String {
    let dateStr = "\(date.day!)-\(date.month!)-\(date.year!) \(date.hour!):\(date.minute!):\(date.second!)"
    let df = DateFormatter()
    df.dateFormat = "dd-MM-yyyy HH:mm:ss"
    df.setLocal()
    let date1 = df.date(from: dateStr)
    df.dateFormat = "dd-MMM-yyyy hh:mm a"
    return df.string(from: date1!)
  }
  
  //TODO:- It will used to get inner data of ECG machine from data.
  @objc func getECGInnerDetails(dateCArray:[DateComponents],index: Int) {
    
    ECGReadData.getInstance().dailyCheckDetail(dateCArray[index])
    ECGReadData.getInstance().blockECGDataComplition = {(success, data, type) -> Void in
      //TODO:- It will used to get inner data of ECG machine from data.
      if let innerData = data as? ECGInfoItem_InnerData {
        
        var arrecgContent = ""
        
        if let arr = innerData.arrEcgContent as? [Double] {
          arrecgContent = arr.compactMap({"\($0)"}).joined(separator: ",")
        }
        
        var arrEcgHeartRate = ""
        if let arr = innerData.arrEcgHeartRate as? [Double] {
          arrEcgHeartRate = arr.compactMap({"\($0)"}).joined(separator: ",")
        }
        
        let arrFilter = self.ecgDataArray.filter({$0.readingDateTime == self.getDateStrFromDateComponent(date: dateCArray[index])})
        if arrFilter.isEmpty {
          self.saveECGDataToCoreData(ecg: ECGCheckmeData(arr_ecg_content: arrecgContent, arr_ecg_heartrate: arrEcgHeartRate, ecg_result: "\(ECGInfoItem.pvcsDescrib(innerData.pvcs)!)", hr: "\(innerData.hr)", qrs: "\(innerData.qrs)", qt: "\(innerData.qt)", qtc: "\(innerData.qTc)", readingDateTime: self.getDateStrFromDateComponent(date: dateCArray[index]), selectedUserID: "", selectUserName: "", selectedUserPhone: "", deviceUser: "", deviceIdentifire: "", readingNotes: ""))
        }
        
        if dateCArray.count == index+1 {
//          Hide Loader
//          print("ECG DATA =======")
//          print("              ||")
//          print("              ||")
//          print("              ||")
//          print("              =======>", self.mappedEcgDATA)
          
          //TODO: Sending Collection to RN.
          let data: [String: Any?] = ["data": self.mappedEcgDATA, "device": self.mapDeviceDescription(((BTUtils.getInstance()?.currentPeripheral.peripheral)!))]
          
          self.emitEvent(Self.DATA_EVENT, withData: data)
          
          self.emitEvent(Self.COLLECTION_FINISHED_EVENT, withData: nil)
          
          self.arrNewECGData = self.ecgDataArray.filter({$0.selectedUserID.trimmingCharacters(in: CharacterSet.whitespaces).isEmpty && $0.hr.ns.intValue > 0 && $0.qt.ns.intValue > 0 && $0.qtc.ns.intValue > 0 && $0.qrs.ns.intValue > 0 }).reversed()
          if !self.arrNewECGData.isEmpty {
            //Refresh List in Flatlist
            print("New Data", self.arrNewECGData)
          }
        }
        else {
          self.getECGInnerDetails(dateCArray: dateCArray, index: index+1)
        }
      }
    }
  }
  
  @objc func startScan(_ resolve: RCTPromiseResolveBlock,
                       rejecter reject: RCTPromiseRejectBlock) {
    
    NotificationCenter.default.addObserver(self, selector: #selector(findPeriphral(nrf:)), name: NSNotification.Name(rawValue: FINDPERIPHRAL), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(onConnectCheckmeSuccessNtf(nrf:)), name: NSNotification.Name(rawValue: CONNECTED), object: nil)
    
    do {
      print(Self.MODULE_NAME, "Start scanning for devices.")
      BTUtils.getInstance()!.beginScan()

//        self.emitEvent(Self.DEVICE_FOUND_EVENT, withData: self.mapDeviceDescription(device))
//        self.emitEvent(Self.AMBIGUOUS_DEVICE_FOUND_EVENT, withData: self.mapDeviceDescription(d))
//        self.emitEvent(Self.SCAN_FINISHED_EVENT, withData: nil)
//      )
      try ObjC.catchException {
       
      }
      resolve("Scanning Devices")
    } catch {
      NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: FINDPERIPHRAL), object: nil)
      NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: CONNECTED), object: nil)
      reject(Self.UNKNOWN_ERROR, Self.UNKOWN_ERROR_MSG, error)
    }
  }
  
  @objc func startCollection(_ resolve: RCTPromiseResolveBlock,
                             rejecter reject: RCTPromiseRejectBlock) {
    do {
      print(Self.MODULE_NAME, "Start data collection.")

      //IF DATA FOUND
//          self.emitEvent(Self.DATA_EVENT, withData: data)
      //DATA SCAN FINISHED
//          self.emitEvent(Self.COLLECTION_FINISHED_EVENT, withData: nil)
      //DEVICE CONNECT EVENT
//          self.emitEvent(Self.DEVICE_CONNECTED_EVENT, withData: self.mapDeviceDescription(device))
      //DEVICE DISCONNECT EVENT
//          self.emitEvent(Self.DEVICE_DISCONNECTED_EVENT, withData: self.mapDeviceDescription(device))
      
      try ObjC.catchException {
        self.syncDataFromDevice("ecg")
      }
      resolve(nil)
    } catch {
      reject(Self.UNKNOWN_ERROR, Self.UNKOWN_ERROR_MSG, error)
    }
  }
  
  @objc func connectToDevice(_ uuid: String,
                             resolver resolve: RCTPromiseResolveBlock,
                             rejecter reject: RCTPromiseRejectBlock) {
    do {
      
      try ObjC.catchException {
        let peripheral = self.arrBLEList.filter({ (item) -> Bool in
          return item.identifier.description.contains(uuid ?? "")
        })
        print(Self.MODULE_NAME, "Connect device with \(peripheral[0].name) address.")
        BTUtils.getInstance()!.connect(to: peripheral[0])
      }
      resolve(nil)
    } catch {
      reject(Self.UNKNOWN_ERROR, Self.UNKOWN_ERROR_MSG, error)
    }
  }
  
  @objc func stopScan(_ resolve: RCTPromiseResolveBlock,
                      rejecter reject: RCTPromiseRejectBlock) {
    do {
      print(Self.MODULE_NAME, "Stop scanning for devices.")
      try ObjC.catchException {
        BTUtils.getInstance()!.stopScan()
      }
      resolve(nil)
    } catch {
      reject(Self.UNKNOWN_ERROR, Self.UNKOWN_ERROR_MSG, error)
    }
  }
  
  
  override func supportedEvents() -> [String]! {
    return [
      Self.DATA_EVENT, Self.DEVICE_FOUND_EVENT, Self.DEVICE_CONNECTED_EVENT,
      Self.DEVICE_DISCONNECTED_EVENT, Self.AMBIGUOUS_DEVICE_FOUND_EVENT,
      Self.SCAN_FINISHED_EVENT, Self.COLLECTION_FINISHED_EVENT
    ]
  }
  
  override static func requiresMainQueueSetup() -> Bool {
    return true
  }
}
