//
//  EcgKitBridge.m
//  EcgDemo
//
//  Created by LN-iMAC-004 on 01/11/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTEventEmitter.h>
#import <React/RCTBridgeModule.h>

@interface RCT_EXTERN_MODULE(EcgKit, RCTEventEmitter)

RCT_EXTERN_METHOD(
                  initialize: (RCTPromiseResolveBlock)resolve
                  rejecter: (RCTPromiseRejectBlock)reject
                  )

RCT_EXTERN_METHOD(
                  stopScan: (RCTPromiseResolveBlock)resolve
                  rejecter: (RCTPromiseRejectBlock)reject
                  )

RCT_EXTERN_METHOD(
                  startScan: (RCTPromiseResolveBlock)resolve
                  rejecter: (RCTPromiseRejectBlock)reject
                  )

RCT_EXTERN_METHOD(
                  connectToDevice: (NSString)address
                  resolver: (RCTPromiseResolveBlock)resolve
                  rejecter: (RCTPromiseRejectBlock)reject
                  )

RCT_EXTERN_METHOD(
                  connectD: (RCTPromiseResolveBlock)resolve
                  rejecter: (RCTPromiseRejectBlock)reject
                  )

RCT_EXTERN_METHOD(
                  startCollection: (RCTPromiseResolveBlock)resolve
                  rejecter: (RCTPromiseRejectBlock)reject
                  )

@end

//@interface RCT_EXTERN_MODULE(BleScan, NSObject)
//
//RCT_EXTERN_METHOD(fetchApproovToken:(NSString *)url
//                  resolver:(RCTPromiseResolveBlock)resolve
//                  rejecter:(RCTPromiseRejectBlock)reject
//                  )
//
//RCT_EXTERN_METHOD(findPeriphral: (RCTPromiseResolveBlock)resolve
//                       rejecter: (RCTPromiseRejectBlock)reject
//                  )
//
//@end
