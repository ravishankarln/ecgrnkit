//
//  NIBPInfoItem.h
//  Checkme Mobile
//
//  Created by Viatom on 2018/1/3.
//  Copyright © 2018年 VIATOM. All rights reserved.
//

#import "MeasureInfoBase.h"
#import "TypesDef.h"

@interface NIBPInfoItem : MeasureInfoBase

@property (nonatomic, assign) U16 sysValue;
@property (nonatomic, assign) U8  diaValue;
@property (nonatomic, assign) U8  prValue;

@end
