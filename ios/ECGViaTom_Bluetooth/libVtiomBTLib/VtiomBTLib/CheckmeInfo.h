//
//  CheckmeInfo.h
//  Checkme Mobile
//
//  Created by Joe on 14/11/11.
//  Copyright (c) 2014年 VIATOM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CheckmeInfo : NSObject

@property(nonatomic,retain) NSString* region;
@property(nonatomic,retain) NSString* model;
@property(nonatomic,retain) NSString* hardware;
@property(nonatomic,retain) NSString* software;
@property(nonatomic,retain) NSString* language;
@property(nonatomic,retain) NSString* theCurLanguage;
@property(nonatomic,retain) NSString* sn;
@property (nonatomic, copy) NSString *Application;

@property (nonatomic, retain) NSString *SPCPVer;
@property (nonatomic, retain) NSString *FileVer;

-(instancetype)initWithJSONStr:(NSData*) data;
@end
