//
//  ECGReadData.m
//  ViatomConnect
//
//  Created by LN-MCBK-004 on 28/11/18.
//  Copyright © 2018 LetsNurture. All rights reserved.
//

#import "ECGReadData.h"

@interface ECGReadData()
@property (nonatomic, strong) CheckmeInfo *info;

@property (nonatomic, strong) User *curUser;   //

@property (nonatomic, strong) DailyCheckItem *item; //
@property (nonatomic, strong) NSArray *arrUserList; //
@property (nonatomic, strong) NSArray *arrCheckListItems; //
@property (nonatomic, strong) NSArray *arrCheckListItemDetails; //
@property (nonatomic, strong) NSArray *arrTemperatureList; //
@property (nonatomic, strong) NSArray *arrSPO2List; //
@property (nonatomic, strong) NSArray *arrBPList; //
@property (nonatomic, strong) NSArray *arrECGList; //
@end

@implementation ECGReadData

+(ECGReadData *)GetInstance
{
    static ECGReadData *inst = nil;
    if(!inst){
        inst = [[ECGReadData alloc] init];
    }
    return inst;
}

-(id)init
{
    self = [super init];
    if(self){
        [BTCommunication sharedInstance].delegate = self;
    }
    return self;
}

- (void)readCheckmeInfo{
    
    [[BTCommunication sharedInstance] BeginGetInfo];
}

//  ping
- (void)ping {
    [[BTCommunication sharedInstance] BeginPing];
}

- (void)syncTime {
    [[BTCommunication sharedInstance] upDataTime];
}

- (void)readAllUser{
    
    //NSLog(@"FILE_Type_UserList: %d",FILE_Type_UserList);
    [[BTCommunication sharedInstance] BeginReadFileWithFileName:Home_USER_LIST_FILE_NAME fileType:FILE_Type_UserList];
    
    
}


//  read dailyCheck list data
- (void)dailyCheck {
    
    NSString *fileName = [NSString stringWithFormat:DLC_LIST_FILE_NAME,_curUser.ID];
    [[BTCommunication sharedInstance] BeginReadFileWithFileName:fileName fileType:FILE_Type_DailyCheckList];
}

-(void)temperatureData{
    NSString *fileName = [NSString stringWithFormat:TEMP_FILE_NAME];
    [[BTCommunication sharedInstance] BeginReadFileWithFileName:fileName fileType:FILE_Type_TempList];
}

-(void)heartBeatData{
    NSString *fileName = [NSString stringWithFormat:BPCheck_FILE_NAME];
    [[BTCommunication sharedInstance] BeginReadFileWithFileName:fileName fileType:FILE_Type_BPCheckList];
}

-(void)spo2Data{
    NSString *fileName = [NSString stringWithFormat:SPO2_FILE_NAME];
    [[BTCommunication sharedInstance] BeginReadFileWithFileName:fileName fileType:FILE_Type_SPO2List];
}

-(void)ecgData{
    NSString *fileName = [NSString stringWithFormat:ECG_LIST_FILE_NAME];
    [[BTCommunication sharedInstance] BeginReadFileWithFileName:fileName fileType:FILE_Type_EcgList];
}

// read ped list
- (void)ped {
    
    NSString* fileName = [NSString stringWithFormat:PED_FILE_NAME,_curUser.ID];
    [[BTCommunication sharedInstance] BeginReadFileWithFileName:fileName fileType:FILE_Type_DailyCheckList];
}


// read dailyCheck detail data
- (void)dailyCheckDetail:(NSDateComponents *)dtcDate{
    
    [[BTCommunication sharedInstance] BeginReadFileWithFileName:[self makeDateFileName:dtcDate fileType:FILE_Type_EcgDetailData] fileType:FILE_Type_EcgDetailData];
}


-(NSString *)makeDateFileName:(NSDateComponents *)date fileType:(U8)type
{
    NSString* dateString = [NSString stringWithFormat:@"%ld%02ld%02ld%02ld%02ld%02ld",(long)date.year,(long)date.month,(long)date.day,(long)date.hour,date.minute,(long)date.second];
    
    if (type==FILE_Type_ECGVoiceData || type == FILE_Type_SpcVoiceData) {
        NSString* voiceString = [NSString stringWithFormat:@"%@.wav",dateString];
        return voiceString;
    }
    return dateString;
}

#pragma mark  --  delegate

- (void)getInfoSuccessWithData:(NSData *)data{
    _info = [[CheckmeInfo alloc] initWithJSONStr:data];
    //NSLog(@"_info: %@",_info);
}

- (void)getInfoFailed{
    //NSLog(@"getInfoFailed");
}

- (void)pingSuccess{
    //NSLog(@"Ping success");
}

- (void)pingFailed{
    //NSLog(@"pingFailed");
}

- (void)updataTimeSuccess{
    //NSLog(@"updataTimeSuccess");
}

- (void)updataTimeFailed{
    //NSLog(@"updataTimeFailed");
}

- (void)readCompleteWithData:(FileToRead *)fileData{
    
    //NSLog(@"fileData.fileType==> %hhu",fileData.fileType);
    
    FileToRead *file = fileData;
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    if (file.fileType == FILE_Type_xuserList) {    //  是xusr.dat数据
        if(file.enLoadResult != kFileLoadResult_NotExist)
        {
            if(file.enLoadResult != kFileLoadResult_TimeOut)
            {
                NSArray *newList = [FileParser paserXusrList_WithFileData:file.fileData];
                if (self.blockECGDataComplition != nil) {
                    self.blockECGDataComplition(TRUE, newList, fileData.fileType);
                }
                //NSLog(@"newList: %@",newList);
                
            }
        }
    } else if (file.fileType == FILE_Type_UserList) {
        
        if(file.enLoadResult != kFileLoadResult_NotExist)
        {
            NSArray *newList = [FileParser parseUserList_WithFileData:file.fileData];
            //NSLog(@"%@",newList);
            if (newList.count >= 1){
                _curUser = newList[1];   // The second test user has data. You can selected your's test user that has data.
            }
            else if (newList.count > 0){
                _curUser = newList[0];   // The second test user has data. You can selected your's test user that has data.
            }
            else{
                
            }
            if (self.blockECGDataComplition != nil) {
                self.blockECGDataComplition(TRUE, newList, fileData.fileType);
            }
//            //NSLog(@"_curUser.name %@",_curUser.name);
//            //NSLog(@"_curUser.arrSPO2 %@",_curUser.arrSPO2);
//            //NSLog(@"_curUser.arrECG %@",_curUser.arrECG);
//            //NSLog(@"_curUser.arrBPI %@",_curUser.arrBPI);
//            //NSLog(@"_curUser.arrTemp %@",_curUser.arrTemp);
//            //NSLog(@"_curUser.arrBPCheck %@",_curUser.arrBPCheck);
//            //NSLog(@"_curUser.arrDlc %@",_curUser.arrDlc);
//            //NSLog(@"_curUser.arrPed %@",_curUser.arrPed);
//            //NSLog(@"_curUser.arrSLM %@",_curUser.arrSLM);
        }
    }
    
    ///////////
    
    else if(fileData.fileType == FILE_Type_DailyCheckList) {
        if(fileData.enLoadResult != kFileLoadResult_TimeOut){
            
            NSArray *arr = [FileParser parseDlcList_WithFileData:fileData.fileData];
            _item = arr.firstObject;
            if (self.blockECGDataComplition != nil) {
                self.blockECGDataComplition(TRUE, arr, fileData.fileType);
            }

//            for (DailyCheckItem *data in arr){
//                //NSLog(@"_item FILE_Type_DailyCheckList :%@",data);
//                //NSLog(@"_item FILE_Type_DailyCheckList _item.ECG_R:%u",data.ECG_R);
//                //NSLog(@"_item FILE_Type_DailyCheckList _item.dtcDate:%@",data.dtcDate);
//                //NSLog(@"_item FILE_Type_DailyCheckList _item.enPassKind:%u",data.enPassKind);
//                //NSLog(@"_item FILE_Type_DailyCheckList _item.userID:%c",data.userID);
//                //NSLog(@"_item FILE_Type_DailyCheckList _item.innerData:%@",data.innerData);
//                //NSLog(@"_item FILE_Type_DailyCheckList _item.bDownloadIng:%d",data.bDownloadIng);
//                //NSLog(@"_item FILE_Type_DailyCheckList _item.downloadProgress:%f",data.downloadProgress);
//                //NSLog(@"_item FILE_Type_DailyCheckList _item.dataVoice:%@",data.dataVoice);
//                //NSLog(@"_item FILE_Type_DailyCheckList _item.HR:%hu",data.HR);
//                //NSLog(@"_item FILE_Type_DailyCheckList _item.ECG_R:%u",data.ECG_R);
//                //NSLog(@"_item FILE_Type_DailyCheckList _item.SPO2:%hhu",data.SPO2);
//                //NSLog(@"_item FILE_Type_DailyCheckList _item.PI:%f",data.PI);
//                //NSLog(@"_item FILE_Type_DailyCheckList _item.SPO2_R:%u",data.SPO2_R);
//                //NSLog(@"_item FILE_Type_DailyCheckList _item.BP_Flag:%d",data.BP_Flag);
//                //NSLog(@"_item FILE_Type_DailyCheckList _item.BP:%hd",data.BP);
//                //NSLog(@"_item FILE_Type_DailyCheckList _item.BPI_R:%u",data.BPI_R);
//                //NSLog(@"_item FILE_Type_DailyCheckList _item.bHaveVoiceMemo:%d",data.bHaveVoiceMemo);
//            }
            
            
        }
    } else if (fileData.fileType == FILE_Type_EcgDetailData) {
        if (fileData.enLoadResult != kFileLoadResult_TimeOut) {
            
            ECGInfoItem_InnerData *innerData = [FileParser parseEcg_WithFileData:fileData.fileData];
            
            if (self.blockECGDataComplition != nil) {
                if (innerData != nil){
                    self.blockECGDataComplition(TRUE, innerData, fileData.fileType);
                } else {
                    self.blockECGDataComplition(TRUE, [ECGInfoItem_InnerData new], fileData.fileType);
                }
            }
            
//            //NSLog(@"_item.innerData FILE_Type_EcgDetailData :%@",_item.innerData);
//            //NSLog(@"_item.innerData FILE_Type_EcgDetailData arrEcgContent:%@",_item.innerData.arrEcgContent);
//            //NSLog(@"_item.innerData FILE_Type_EcgDetailData arrEcgHeartRate:%@",_item.innerData.arrEcgHeartRate);
//            //NSLog(@"_item.innerData FILE_Type_EcgDetailData HR:%hu",_item.innerData.HR);
//            //NSLog(@"_item.innerData FILE_Type_EcgDetailData ST:%hd",_item.innerData.ST);
//            //NSLog(@"_item.innerData FILE_Type_EcgDetailData QRS:%hu",_item.innerData.QRS);
//            //NSLog(@"_item.innerData FILE_Type_EcgDetailData PVCs:%hu",_item.innerData.PVCs);
//            //NSLog(@"_item.innerData FILE_Type_EcgDetailData QTc:%hu",_item.innerData.QTc);
//            //NSLog(@"_item.innerData FILE_Type_EcgDetailData ECG_Type:%hhu",_item.innerData.ECG_Type);
//            //NSLog(@"_item.innerData FILE_Type_EcgDetailData ecgResultDescrib:%@",_item.innerData.ecgResultDescrib);
//            //NSLog(@"_item.innerData FILE_Type_EcgDetailData timeLength:%u",_item.innerData.timeLength);
//            //NSLog(@"_item.innerData FILE_Type_EcgDetailData enFilterKind:%u",_item.innerData.enFilterKind);
//            //NSLog(@"_item.innerData FILE_Type_EcgDetailData enLeadKind:%hhu",_item.innerData.enLeadKind);
//            //NSLog(@"_item.innerData FILE_Type_EcgDetailData QT:%hu",_item.innerData.QT);
//            //NSLog(@"_item.innerData FILE_Type_EcgDetailData isQT:%d",_item.innerData.isQT);
        }
    } else if (fileData.fileType == FILE_Type_TempList) {
        
        if (fileData.enLoadResult != kFileLoadResult_TimeOut) {
            NSArray *arr = [FileParser parseTempList_WithFileData:fileData.fileData];
            //NSLog(@"arr FILE_Type_TempList :%@",arr);
            if (self.blockECGDataComplition != nil) {
                self.blockECGDataComplition(TRUE, arr, fileData.fileType);
            }
//            for (TempInfoItem *temp in arr){
//                //NSLog(@"*********************************");
//                //NSLog(@"temp.PTT_Value :%f",temp.PTT_Value);
//                //NSLog(@"temp.measureMode :%hhu",temp.measureMode);
//                //NSLog(@"temp.enPassKind :%u",temp.enPassKind);
//                //NSLog(@"temp.PTT_Value :%d",[temp bMatchWithCondition_TypeKind:10]);
//            }
            
        }
    }
    else if(fileData.fileType == FILE_Type_SPO2List) {
        if (fileData.enLoadResult != kFileLoadResult_TimeOut) {
            NSArray *arr = [FileParser parseSPO2List_WithFileData:fileData.fileData];
            //NSLog(@"************FILE_Type_SPO2List*************");
            if (self.blockECGDataComplition != nil) {
                self.blockECGDataComplition(TRUE, arr, fileData.fileType);
            }
//            for (SPO2InfoItem *data in arr){
//                //NSLog(@"**************************");
//                //NSLog(@"SPO2_Value: %d",data.SPO2_Value);
//                //NSLog(@"PR %hu",data.PR);
//                //NSLog(@"PI: %f",data.PI);
//                //NSLog(@"enPassKind: %u",data.enPassKind);
//                //NSLog(@"result: %u",[data bMatchWithCondition_TypeKind:data.enPassKind]);
//            }
        }
    }
    else if(fileData.fileType == FILE_Type_BPCheckList) {
        if(fileData.enLoadResult != kFileLoadResult_TimeOut){
            //NSLog(@"************FILE_Type_BPCheckList*************");
            NSArray *arr = [FileParser parseBPCheck_WithFileData:fileData.fileData];
            if (self.blockECGDataComplition != nil) {
                self.blockECGDataComplition(TRUE, arr, fileData.fileType);
            }
//            for (BPCheckItem *data in arr){
//                //NSLog(@"**************************");
//                //NSLog(@"userID: %d",data.userID);
//                //NSLog(@"dtcDate %@",data.dtcDate);
//                //NSLog(@"BPIndex: %hu",data.BPIndex);
//                //NSLog(@"rPresure: %u",data.rPresure);
//                //NSLog(@"cPresure: %u",data.cPresure);
//                //NSLog(@"cPresure: %u",data.cPresure);
//            }
        }
    }
    else if(fileData.fileType == FILE_Type_EcgList) {
        if(fileData.enLoadResult != kFileLoadResult_TimeOut){
            //NSLog(@"************FILE_Type_EcgList*************");
            NSArray *arr = [FileParser parseEcgList_WithFileData:fileData.fileData];
            
            if (self.blockECGDataComplition != nil) {
                self.blockECGDataComplition(TRUE, arr, fileData.fileType);
            }
            
//            for (ECGInfoItem *data in arr){
//                //NSLog(@"**************************");
//                //NSLog(@"userID: %c",data.userID);
//                //NSLog(@"dtcDate %@",data.dtcDate);
//
//
//
//                //NSLog(@"_item FILE_Type_DailyCheckList :%@",data);
//
//                //NSLog(@"_item FILE_Type_DailyCheckList _item.dtcDate:%@",data.dtcDate);
//                //NSLog(@"_item FILE_Type_DailyCheckList _item.enPassKind:%u",data.enPassKind);
//                //NSLog(@"_item FILE_Type_DailyCheckList _item.userID:%c",data.userID);
//                //NSLog(@"_item FILE_Type_DailyCheckList _item.innerData:%@",data.innerData);
//                //NSLog(@"_item FILE_Type_DailyCheckList _item.bDownloadIng:%d",data.bDownloadIng);
//                //NSLog(@"_item FILE_Type_DailyCheckList _item.downloadProgress:%f",data.downloadProgress);
//                //NSLog(@"_item FILE_Type_DailyCheckList _item.dataVoice:%@",data.dataVoice);
//                //NSLog(@"_item FILE_Type_DailyCheckList _item.bHaveVoiceMemo:%d",data.bHaveVoiceMemo);
//
//                data.innerData = [FileParser parseEcg_WithFileData:fileData.fileData];
//
//                //NSLog(@"_item.innerData FILE_Type_EcgDetailData arrEcgContent:%@",data.innerData.arrEcgContent);
//                //NSLog(@"_item.innerData FILE_Type_EcgDetailData arrEcgHeartRate:%@",data.innerData.arrEcgHeartRate);
//            }
        }
    }
}

- (void)postCurrentReadProgress:(double)progress{
    
}

- (void)postCurrentWriteProgress:(FileToRead *)fileData{
    
}

@end
