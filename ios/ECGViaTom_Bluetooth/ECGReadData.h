//
//  ECGReadData.h
//  ViatomConnect
//
//  Created by LN-MCBK-004 on 28/11/18.
//  Copyright © 2018 LetsNurture. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BTCommunication.h"
#import "CheckmeInfo.h"
#import "FileParser.h"
#import "User.h"

NS_ASSUME_NONNULL_BEGIN

typedef void (^complitionBlock) (BOOL success, id response, U8 fileType);

@interface ECGReadData : NSObject<BTCommunicationDelegate>
+(ECGReadData *)GetInstance;
@property void (^blockECGDataComplition) (BOOL success, id response, U8 fileType);
- (void)readCheckmeInfo;
- (void)ping;
- (void)syncTime;
- (void)readAllUser;
- (void)dailyCheck;
- (void)temperatureData;
- (void)heartBeatData;
- (void)spo2Data;
- (void)ecgData;
- (void)ped;
- (void)dailyCheckDetail:(NSDateComponents *)dtcDate;
@end

NS_ASSUME_NONNULL_END
