import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TouchableOpacity, ScrollView, ActivityIndicator } from 'react-native';
import EcgKit from './native/EcgKit';

export interface Device {
  id: number;
  address: string;
  name: string;
  state: number;
  modelName: string;
  manufacturer: string;
}

export interface EcgData {
  arr_ecg_content: String;
  arr_ecg_heartrate: String;
  ecg_result: String;
  hr: String;
  qrs: String;
  qt: String;
  qtc: String;
  readingDateTime: String;
  selectedUserID?: String;
  selectUserName?: String;
  selectedUserPhone?: String;
  deviceUser?: String;
  deviceIdentifire?: String;
  readingNotes?: String;
}

export interface Reading {
  data: [EcgData];
  source?: Device;
}

const ecgHandler = new EcgKit()

export default class App extends Component<{}> {
  onDeviceFoundListener;
  onDeviceConnectListener;
  onSearchFinishListener;
  onCollectionFoundListener;
  onScanFinishedListener;
  constructor(){
    super() 
    
    this.state = {
      status: 'starting',
      message: '--',
      devices: [],
      readings: [],
      loading: false
    };

  }

  _handleDeviceFound = (device:Device) => {
    console.log("Device Found", device)
    this.setState({
      status: 'Found Device(s)',
      message: 'Supported Devices',
      devices: [device]
    })
  }

  componentWillUnmount() {
    ecgHandler.removeAllListeners()
  }

  _handleSearchFinish = () => {
    console.log("FINISHED SCANING")
    this.setState({
      status: 'Finished Searching',
      message: 'Supported Devices',
    })
  }

  _handleCollectionData = (reading:Reading) => {
    console.log("DATA AVAILABLE FOR "+ reading.data)
    this.setState({
      status: "DATA AVAILABLE FOR "+ this.state.devices[0].name,
      message: 'ECG DATA',
      readings: reading.data,
      loading: false
    })
  }

  _handleScanFinished = () => {
    console.log("DATA SCAN FINISHED FOR "+ this.state.devices[0].name)
    this.setState({
      status: "DATA SCAN FINISHED FOR "+ this.state.devices[0].name,
      message: 'ECG DATA',
    })
  }

  componentDidMount() {
    this.onDeviceFoundListener = ecgHandler.addListener('deviceFound', this._handleDeviceFound);
    this.onDeviceConnectListener = ecgHandler.addListener('deviceConnected', this._handleDeviceFound);
    this.onSearchFinishListener = ecgHandler.addListener('scanFinished', this._handleSearchFinish);
    this.onCollectionFoundListener = ecgHandler.addListener('data', this._handleCollectionData);
    this.onScanFinishedListener = ecgHandler.addListener('collectionFinished', this._handleScanFinished);

    ecgHandler.initialize()
    .then(res => {
      console.log("Bluetooth Status", res)
      this.setState({
        status: 'BlueTooth'+ res.init,
        message: "Starting Bluetooth"
      });

      ecgHandler.startScan()
      .then((message) => {
        // You're good to go.
        this.setState({
          status: 'Starting bluetooth scan',
          message
        });
        console.log("Message", message)
      })
      .catch(() => {
        // Don't forget to provide a licence key!
      });

    })
    .catch(error => {
      console.log(error)
      this.setState({
        status: 'BlueTooth Off(Not Sure)',
        message: "Failed to initialize bluetooth"
      });
    })

  }

  _connect = (item) => {
    console.log('id ======>', item )
    if (item.state === 2) {
      alert("Fetching Data")
      ecgHandler.startCollection()
      this.setState({
        status: 'Fetching data for ' + this.state.devices[0].name,
        message: "Supported Devices",
        loading: true
      });
      return
    } else {
      ecgHandler.connectToDevice(item)
    }
    
  }

  currentState = (state) => {
    switch (state) {
      case 0:
          return "Connect"
        break;
      case 1:
          return "Connecting"
        break;
      case 2:
          // return "Connected"
          return "Pull data"
        break;
      case 3:
          return "Disconnecting"
        break;
      default:
        return "Connect"
        break;
    }
  }

  _renderLoader = () => {
    return (
      <View style={{ ...StyleSheet.absoluteFill, backgroundColor: "rgba(0,0,0,0.5)", alignItems: "center", justifyContent: "center" }} >
        <ActivityIndicator  color="white" size="large" />
      </View>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>☆EcgKit example☆</Text>
        <Text style={styles.instructions}>STATUS: {this.state.status}</Text>
        <Text style={styles.welcome}>☆NATIVE CALLBACK MESSAGE☆</Text>
        <Text style={styles.instructions}>{this.state.message}</Text>

        <View style={{ width: "100%" }} >
          {
            this.state.devices.map((item, index)=> {
              return(
                <View key={index} style={{ padding: 10, margin: 10, marginBottom: 5, borderRadius: 5, borderWidth: 0.5, flexDirection: "row" }} >
                  <View style={{ flex: 1, flexDirection: "column" }} >
                  <Text> {item.name} </Text>
                  <Text> {item.id} </Text>
                  </View>
                  <TouchableOpacity
                    onPress={() => this._connect(item)}
                    hitSlop={{ left: 20, right: 20, top: 20, bottom: 20 }} >
                  <Text style={{ width: 80 }} > {this.currentState(item.state)}  </Text>
                  </TouchableOpacity>
                </View>
              )
            })
          }
        </View>
        <ScrollView style={{ width: "100%" }}>
          {
            this.state.readings.map((item,index) => {
              return (
                <View key={index} style={{ padding: 10, margin: 10, marginBottom: 5, borderRadius: 5, borderWidth: 0.5, flexDirection: "row" }} >
                  <View style={{ flex: 1, flexDirection: "column" }} >
                  <Text> {"HR: "}{item.hr} </Text>
                  <Text> {"QT: "}{item.qt} </Text>
                  </View>
                  <Text style={{ width: 80 }} > {item.ecg_result}  </Text>
                </View>
              )
            })
          }
          </ScrollView>
        {this.state.loading && this._renderLoader()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
