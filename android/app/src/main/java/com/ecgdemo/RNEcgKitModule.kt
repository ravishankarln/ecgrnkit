package com.ecgdemo

import android.app.Service
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.*
import android.os.IBinder
import android.widget.Toast

import android.util.Log
import com.facebook.react.bridge.*
import com.facebook.react.modules.core.DeviceEventManagerModule

import com.viatom.checkmelib.bluetooth.BTConnectListener
import com.viatom.checkmelib.bluetooth.BTUtils
import com.viatom.checkmelib.bluetooth.GetInfoThreadListener
import com.viatom.checkmelib.bluetooth.ReadFileListener
import com.viatom.checkmelib.measurement.ECGInnerItem
import com.viatom.checkmelib.measurement.ECGItem
import com.viatom.checkmelib.measurement.MeasurementConstant
import com.viatom.checkmelib.utils.ClsUtils
import androidx.appcompat.app.AlertDialog

import java.util.*


class RNEcgKitModule (
        private val reactContext: ReactApplicationContext
) : ReactContextBaseJavaModule(reactContext), BTConnectListener,GetInfoThreadListener, ReadFileListener {
    companion object {
        const val MODULE_NAME = "EcgKit"

        const val INIT_ERROR = "INIT_ERROR"
        const val PAIR_ERROR = "PAIR_ERROR"
        const val UNKNOWN_ERROR = "UNKNOWN_ERROR"

        const val DATA_EVENT = "data"
        const val DEVICE_FOUND_EVENT = "deviceFound"
        const val DEVICE_CONNECTED_EVENT = "deviceConnected"
        const val DEVICE_DISCONNECTED_EVENT = "deviceDisconnected"
        const val AMBIGUOUS_DEVICE_FOUND_EVENT = "ambiguousDeviceFound"
        const val SCAN_FINISHED_EVENT = "scanFinished"
        const val COLLECTION_FINISHED_EVENT = "collectionFinished"
    }

    override fun getName() = MODULE_NAME

    override fun getConstants() = mapOf(
            "EVENTS" to listOf(
                    DATA_EVENT, DEVICE_FOUND_EVENT, DEVICE_CONNECTED_EVENT,
                    DEVICE_DISCONNECTED_EVENT, AMBIGUOUS_DEVICE_FOUND_EVENT,
                    SCAN_FINISHED_EVENT, COLLECTION_FINISHED_EVENT
            )
    )

    private val eventEmitter by lazy {
        reactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter::class.java)
    }

    val DEVICE_HEART_ECG = 0
    val CHECKME_ID = "Checkme"
    val MEDCHECK_ID = "MedCheck"

//    lateinit var mDialogBluetooth: AlertDialog
    private var mDialogBluetooth: AlertDialog? = null

    private var currentDeviceType = 0
    private var mECGResult = ""
    private var mArrEcgContent = ""
    private var mArrEcgHeartrate = ""
    private var mPiReading = ""
    private var mSpO2Result = ""
    private var measureMode = ""

    private var mDevice: BluetoothDevice? = null
    lateinit var mBtAdapter: BluetoothAdapter
    lateinit var btBinder: BTUtils.BTBinder

    private val mBluetoothDeviceHashMap = HashMap<String, BluetoothDevice>()

    var mConnected = false
    var mIsServiceBind = false
//    private var scannerToken: ScannerStopToken? = null
//    private var collectionToken: CollectorStopToken? = null
//    private var cancellationTokens = mutableListOf<DeviceAddingCancellationToken>()
//
//    private var foundDevices = mutableListOf<IDeviceDescription>()

    private fun showToast(message: String) {
        //TODO: - POSSIBLE CAUSE FOR TOAST ERROR(s)
        Toast.makeText(reactContext, message, Toast.LENGTH_SHORT).show()
    }

    private fun emitEvent(eventName: String, params: Any?) {
        Log.i(MODULE_NAME, "Send '$eventName' event.")
        eventEmitter.emit(eventName, params)
    }

    private fun mapDeviceDescription(device: BluetoothDevice): WritableMap =
            Arguments.makeNativeMap(mapOf(
                    "id" to device.address,
                    "address" to device.address,
                    "name" to device.name
//                    "modelName" to device.modelName,
//                    "manufacturer" to device.manufacturer
            ))


    private fun mapInitDesc(init: Boolean): WritableMap =
            Arguments.makeNativeMap(mapOf(
                    "init" to init
            ))

    var connection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            Log.d(MODULE_NAME, "try to connect")
            mIsServiceBind = true
            btBinder = service as BTUtils.BTBinder
            emitEvent(DEVICE_FOUND_EVENT, mapDeviceDescription(mDevice!!))
            btBinder.interfaceConnect(mDevice!!.address, this@RNEcgKitModule)
        }

        override fun onServiceDisconnected(name: ComponentName) {
            mIsServiceBind = false
            Log.d(MODULE_NAME, "Service Disconnected: $name")
        }

//        override fun onServiceConnected(name: ComponentName, service: IBinder) {
//            Log.d(MODULE_NAME, "try to connect")
//
//            btBinder = service as BTUtils.BTBinder
//            if (mDevice != null)
//                emitEvent(DEVICE_FOUND_EVENT, mapDeviceDescription(mDevice))
////                btBinder.interfaceConnect()
////                btBinder.interfaceConnect(mDevice.getAddress(), this@TakeReadingActivity)
//
//        }

    }

    fun connectBluetoothDevice(device: BluetoothDevice) {

        mDevice = device

        if (mBtAdapter.isDiscovering()) {
            mBtAdapter.cancelDiscovery()
        }

        if (mDevice!!.bondState != BluetoothDevice.BOND_BONDED) {
            android.util.Log.d(MODULE_NAME, "no bonded")
            try {
                ClsUtils.cancelPairingUserInput(mDevice!!.javaClass, mDevice)
                ClsUtils.createBond(mDevice!!.javaClass, mDevice)
                android.util.Log.d(MODULE_NAME, "bonded: ")

            } catch (e: Exception) {
                e.printStackTrace()
                android.util.Log.d(MODULE_NAME, "not bonded: $e")
            }

        } else {
            Log.d(MODULE_NAME, "bonded, try connect")
            val mIntent = Intent()
            mIntent.action = "com.viatom.checkmelib.bluetooth.BTUtils"
            mIntent.setPackage(reactContext.getPackageName())
            reactContext.bindService(mIntent, connection, Service.BIND_AUTO_CREATE)
        }


    }

    private val mReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action

            if (BluetoothDevice.ACTION_BOND_STATE_CHANGED == action) {

                // pair
                android.util.Log.d(MODULE_NAME, "paired")
                val mIntent = Intent("com.viatom.checkmelib.bluetooth.BTUtils")
                mIntent.setPackage(reactContext.getPackageName())
//                mIntent.setPackage(getActivity().getPackageName())
                reactContext.bindService(mIntent, connection, Service.BIND_AUTO_CREATE)
//                getActivity().bindService(mIntent, connection, Service.BIND_AUTO_CREATE)
                //                hideDialog();
            } else if (BluetoothDevice.ACTION_ACL_DISCONNECTED == action) {

                if (mConnected) {
                    // disconnected

                    mConnected = false
                    //                    unbindService(connection);// debug warning
                    onConnectFailed(BTConnectListener.ERR_CODE_NORMAL)
                    Log.d(MODULE_NAME, "disconnected")
                }

            } else if (BluetoothDevice.ACTION_FOUND == action) {

                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)
                android.util.Log.d(MODULE_NAME, "device found" + device.name + " ====" + device.address)

                //device.getName().equalsIgnoreCase("Checkme 0038")
                if (device.type == BluetoothDevice.DEVICE_TYPE_CLASSIC && device.name != null
                        && (device.name.contains(CHECKME_ID) || device.name.contains(MEDCHECK_ID)) && !mConnected) {

                    //TODO: send device to RN
                    mBluetoothDeviceHashMap.put(device.address, device)
                }

            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED == action) {
                // discover finished
                Log.d(MODULE_NAME, "discover finished")
                mIsServiceBind = false

                if (!mBluetoothDeviceHashMap.isEmpty() && mDevice == null) {
                    connectBluetoothDevice(mBluetoothDeviceHashMap.values.toTypedArray()[0] as BluetoothDevice)
                } else {
                    Log.e(MODULE_NAME, "Some thing went wrong")
//                    getActivity().runOnUiThread(Runnable {
//                        Log.e(TAG, getString(R.string.some_thing_went_wrong))
//                        //                            showToast(getString(R.string.some_thing_went_wrong));
//                        hideDialog()
//                    })
                }

            }
        }
    }

    fun handleEcgList(bytes: ByteArray) {
        val ecgItems = ECGItem.getEcgItemList(bytes)
        Log.d(MODULE_NAME, "ecgitem list size: " + (ecgItems?.size ?: "0"))
        /*ArrayList<String> listItems = new ArrayList<String>();
        for (ECGItem item:ecgItems) {
            listItems.add(item.getDate().toString());
        }*/

        if (ecgItems != null && ecgItems.size > 0) {
            mECGResult = ecgItems[ecgItems.size - 1].imgResult.toString()
            downloadEcgDataDetail(ecgItems[ecgItems.size - 1].date)
        }

    }

    fun makeDateFileName(date: Date, fileType: Int): String {

        val dateString = DateTimeUtils.getFormattedDate("yyyyMMddHHmmss", date)

        return if (fileType == MeasurementConstant.CMD_TYPE_VOICE.toInt()) {
            dateString + ".wav"
        } else dateString

    }

    fun downloadEcgDataDetail(date: Date) {
        if (!mConnected) {
            showToast("Connect Device")
            return
        }

        val fileName = makeDateFileName(date, MeasurementConstant.CMD_TYPE_ECG_NUM.toInt())

        btBinder.interfaceReadFile(fileName, MeasurementConstant.CMD_TYPE_ECG_NUM, 10000, this)
    }

    fun handleEcgDataDetail(bytes: ByteArray) {

        val ecgInnerItem = ECGInnerItem(bytes)
        mArrEcgContent = Arrays.toString(ecgInnerItem.ecgData)
        mArrEcgHeartrate = Arrays.toString(ecgInnerItem.ecgDat)

//            edtHR.setText(ecgInnerItem.hr.toString())
//            edtQRS.setText(ecgInnerItem.qrs.toString())
//            edtQT.setText(ecgInnerItem.qt.toString())
//            edtQTCReading.setText(ecgInnerItem.qTc.toString())

        Log.d(MODULE_NAME, "Scanning... $ecgInnerItem")

//        runOnUiThread {
////            mLlScanLayout.setVisibility(View.GONE)
////            mLlReadingLayout.setVisibility(View.VISIBLE)
//            //        mTvConnectionState.setText(mMultiLanguageSupport.getLabel(UILabelsKeys.CLEAR_SUCCESSFULLY_COMPLETED));
////            Log.d(MODULE_NAME, "Data recieved")
////            mLlReadingFunctionLayout.setVisibility(View.VISIBLE)
////            clearValues() used by android to clear view
////            setViewsAccessibility(false)
//
//            mArrEcgContent = Arrays.toString(ecgInnerItem.ecgData)
//            mArrEcgHeartrate = Arrays.toString(ecgInnerItem.ecgDat)
//
////            edtHR.setText(ecgInnerItem.hr.toString())
////            edtQRS.setText(ecgInnerItem.qrs.toString())
////            edtQT.setText(ecgInnerItem.qt.toString())
////            edtQTCReading.setText(ecgInnerItem.qTc.toString())
//
//            Log.d(MODULE_NAME, "Scanning... $ecgInnerItem")
//
//            //Readings of a single ecg object
////            mRepo = Repo(String.valueOf(UserPreference.getInstance(this@TakeReadingActivity).getId()), mPatienID, "6",
////                    "ecg_data", edtQRS.getText().toString(), edtHR.getText().toString(), edtQT.getText().toString(),
////                    edtQTCReading.getText().toString(), mECGResult, mArrEcgContent, mArrEcgHeartrate, edtDate.getText().toString())
//        }
    }

    private fun isBluetoothOn(): Boolean {
        return mBtAdapter != null && mBtAdapter.isEnabled()
    }

    fun readData(fileName: String, fileType: Byte, fileBuf: ByteArray) {
        when (fileType) {
            MeasurementConstant.CMD_TYPE_ECG_LIST -> handleEcgList(fileBuf)
//            MeasurementConstant.CMD_TYPE_SPO2 -> handleSPO2List(fileBuf)
//            MeasurementConstant.CMD_TYPE_TEMP -> handleTempList(fileBuf)
            MeasurementConstant.CMD_TYPE_ECG_NUM -> handleEcgDataDetail(fileBuf)
            else -> {
            }
        }
    }

    fun downloadEcgList() {
        if (!mConnected) {
            showToast("Please connect device")//"please connect device"
            return
        }

        btBinder.interfaceReadFile(MeasurementConstant.FILE_NAME_ECG_LIST, MeasurementConstant.CMD_TYPE_ECG_LIST, 10000, this)
    }

    override fun onConnectSuccess() {
        Log.d(MODULE_NAME, "Connect Success")
        mConnected = true

        emitEvent(DEVICE_FOUND_EVENT, mapDeviceDescription(mDevice!!))
//        runOnUiThread { mTvConnectionState.setText("Fetching") }
//        btBinder.interfaceGetInfo(10000, this@TakeReadingActivity)
    }

    override fun onGetInfoSuccess(checkmeInfo: String) {
        Log.d(MODULE_NAME, "get info success $checkmeInfo")

        if (currentDeviceType == DEVICE_HEART_ECG) {
            downloadEcgList()
        } else {
            Log.d(MODULE_NAME, "No data for ecg")
        }
    }



    override fun onGetInfoFailed(errCode: Byte) {
        Log.d(MODULE_NAME, "get info failed $errCode")
    }

    override fun onReadPartFinished(fileName: String, fileType: Byte, percentage: Float) {
        Log.d(MODULE_NAME, "Scanning... $percentage")
    }

    override fun onReadSuccess(fileName: String, fileType: Byte, fileBuf: ByteArray) {
        readData(fileName, fileType, fileBuf)
    }

    override fun onReadFailed(fileName: String, fileType: Byte, errCode: Byte) {
        Log.d(MODULE_NAME, "Not able to read data $errCode")
    }

    override fun onConnectFailed(errorCode: Byte) {
        Log.d(MODULE_NAME, "Connect Failed$errorCode")
        //TODO: Find use/class for unbindService
//        unbindService(connection)
    }

    private fun enableBluetoothDialog() {

        if (mDialogBluetooth == null) {
//            mDialogBluetooth = AppUtils.showDialog(reactContext, "Bluetooth",
//                    "Please turn on bluetooth",
//                    "Enable", object : OnDialogClickListener {
//                override fun onDialogClick(dialog: DialogInterface, buttonType: Int) {
//                    if (buttonType == OnDialogClickListener.BUTTON_POSITIVE) {
//                        if (mBtAdapter != null && !mBtAdapter.isEnabled()) {
//                            mBtAdapter.enable()
//                        }
//                    } else if (buttonType == OnDialogClickListener.BUTTON_NEGATIVE) {
//                    }
//                }
//            })
        } else if (!mDialogBluetooth!!.isShowing()) {
            mDialogBluetooth!!.show()
        }

    }

    fun initReceiver() {
        val filter = IntentFilter(BluetoothDevice.ACTION_FOUND)
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)
        filter.addAction(BluetoothDevice.ACTION_PAIRING_REQUEST)
        filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED)
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED)
        reactContext.registerReceiver(mReceiver, filter)
    }

    fun searchBT() {

//        mBtAdapter = BluetoothAdapter.getDefaultAdapter()

        if (mBtAdapter.isDiscovering) {
            mBtAdapter.cancelDiscovery()
        }

        mBtAdapter.startDiscovery()
        android.util.Log.d(MODULE_NAME, "start discover")
    }

    @ReactMethod
    fun initialize(promise: Promise) {
        try {
//            MedMDeviceKit.init(reactContext.currentActivity?.application, key)
            //Start Bluetooth

            promise.resolve(mapInitDesc(true))
        } catch (e: Exception) {
            promise.reject(INIT_ERROR, e)
        }
    }

    @ReactMethod
    fun startScan(promise: Promise) {
        try {
            Log.i(MODULE_NAME, "Start scanning for devices.")
//            mBluetoothAdapter = BaseApplication.getmBluetoothAdapter()
            mBtAdapter = BluetoothAdapter.getDefaultAdapter()
            if (isBluetoothOn()) {
                    //TODO: Show ActivityIndicator in JS File
                    initReceiver()
                    searchBT()

            } else {
                showToast("Please enable bluetooth and try again")
            }

//            scannerToken = MedMDeviceKit.getScanner().start(object : IScannerCallback {
//                override fun onDeviceFound(device: IDeviceDescription) {
//                    foundDevices.add(device)
//                    emitEvent(DEVICE_FOUND_EVENT, mapDeviceDescription(device))
//                }
//
//                override fun onAmbiguousDeviceFound(devices: Array<IDeviceDescription>) {
//                    for (d in devices) {
//                        emitEvent(AMBIGUOUS_DEVICE_FOUND_EVENT, mapDeviceDescription(d))
//                    }
//                }
//
//                override fun onScanFinished() {
//                    emitEvent(SCAN_FINISHED_EVENT, null)
//                }
//            })
            promise.resolve("Scanning Devices")
        } catch (e: Exception) {
            promise.reject(UNKNOWN_ERROR, e)
        }
    }

    @ReactMethod
    fun stopScan(promise: Promise) {
        try {
            Log.i(MODULE_NAME, "Stop scanning for devices.")
//            scannerToken?.stopScan()
            promise.resolve(null)
        } catch (e: Exception) {
            promise.reject(UNKNOWN_ERROR, e)
        }
    }

//    @ReactMethod
//    fun addDevice(sku: Int, promise: Promise) {
//        try {
//            val callback = object : IAddDeviceCallback {
//                override fun onFailure(device: IDeviceDescription) {
//                    val deviceString = mapDeviceDescription(device).toString()
//                    promise.reject(PAIR_ERROR,
//                            Exception("The following device could not be paired: $deviceString"))
//                }
//
//                override fun onSuccess(device: IDeviceDescription) {
//                    promise.resolve(null)
//                }
//            }
//            val device = foundDevices.find { it.sku == sku }!!
//            Log.i(MODULE_NAME, "Pair ${device.modelName} device with ${device.sku} SKU.")
//            cancellationTokens.add(MedMDeviceKit.getDeviceManager().addDevice(device, callback))
//        } catch (e: Exception) {
//            promise.reject(UNKNOWN_ERROR, e)
//        }
//    }
//
//    @ReactMethod
//    fun removeDevice(address: String, promise: Promise) {
//        try {
//            Log.i(MODULE_NAME, "Remove device with $address address.")
//            MedMDeviceKit.getDeviceManager().removeDevice(address)
//            promise.resolve(null)
//        } catch (e: Exception) {
//            promise.reject(UNKNOWN_ERROR, e)
//        }
//    }

//    @ReactMethod
//    fun listDevices(promise: Promise) {
//        try {
//            val devices = MedMDeviceKit.getDeviceManager().devicesList.map { mapDeviceDescription(it) }
//            promise.resolve(Arguments.makeNativeArray<WritableMap>(devices.toTypedArray()))
//        } catch (e: Exception) {
//            promise.reject(UNKNOWN_ERROR, e)
//        }
//    }
//
//    @ReactMethod
//    fun cancelPairings(promise: Promise) {
//        try {
//            Log.i(MODULE_NAME, "Cancel all pairings.")
//            for (token in cancellationTokens) token.cancel()
//        } catch (e: Exception) {
//            promise.reject(UNKNOWN_ERROR, e)
//        }
//    }

    @ReactMethod
    fun startCollection(promise: Promise) {
        try {
            Log.i(MODULE_NAME, "Start data collection.")
//            collectionToken = MedMDeviceKit.getCollector().start(
//                    object : IDataCallback {
//                        override fun onNewData(device: IDeviceDescription?, data: String) {
//                            val deviceMap = if (device != null) mapDeviceDescription(device) else null
//                            emitEvent(DATA_EVENT, Arguments.makeNativeMap(mapOf(
//                                    "data" to data,
//                                    "device" to deviceMap
//                            )))
//                        }
//
//                        override fun onDataCollectionStopped() {
//                            emitEvent(COLLECTION_FINISHED_EVENT, null)
//                        }
//                    },
//                    object : IDeviceStatusCallback {
//                        override fun onConnected(device: IDeviceDescription) {
//                            emitEvent(DEVICE_CONNECTED_EVENT, mapDeviceDescription(device))
//                        }
//
//                        override fun onDisconnected(device: IDeviceDescription) {
//                            emitEvent(DEVICE_DISCONNECTED_EVENT, mapDeviceDescription(device))
//                        }
//                    }
//            )
            promise.resolve(null)
        } catch (e: Exception) {
            promise.reject(UNKNOWN_ERROR, e)
        }
    }

    @ReactMethod
    fun stopCollection(promise: Promise) {
        try {
            Log.i(MODULE_NAME, "Stop data collection.")
//            collectionToken?.stopCollect()
            promise.resolve(null)
        } catch (e: Exception) {
            promise.reject(UNKNOWN_ERROR, e)
        }
    }
}